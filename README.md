# PEB - Practicas

[![GPLv3 license](https://img.shields.io/gitlab/license/appuchia/appuchia-udc/peb-practicas?style=flat-square)](https://gitlab.com/appuchia/appuchia-udc/peb-practicas/blob/master/LICENSE)
[![Author](https://img.shields.io/badge/Project%20by-Appu-9cf?style=flat-square)](https://gitlab.com/appuchia)
![Issues](https://img.shields.io/gitlab/issues/open/appuchia/appuchia-udc/peb-practicas?style=flat-square)

## Qué es esto

Aquí está el código que he escrito en las clases prácticas de Probabilidad y Estadística básica.

## Licencia

El código está licenciado bajo la [licencia GPLv3](https://gitlab.com/appuchia/appuchia-udc/peb-practicas/-/blob/master/LICENSE).

Coded with 🖤 by Appu
